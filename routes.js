function setup (app) {
	// load controllers
	app.get('/', function (req, res) {
		res.sendfile(__dirname + '/client.html');
	});
}

exports.setup = setup;