// hold all sockets
var users = {};
var sockets= {}

function getUsers() {
  var names = [];
  
  
  return names;
}


module.exports = function(io) {

  io.sockets.on('connection', function (socket) {
    socket.emit('new_user', 'hello');

    /////////////////////////////////
    // step 1. handshake and register
    socket.on('register_user', function(data) {
      var username = data.username;
      users[username] = socket;

      var message = username + ' has joined the conversation';
      io.sockets.emit('register_complete', message);
    });

    // setup messaging
    socket.on('message', function(data) {
      console.log('routing message from '+ data.from + ' for ' + data.to);

      if (typeof(users[data.to]) == 'undefined' || users[data.to] == null) {
        console.log(data.to + ' doesnt exist, or is not logged in. try again later.');
      } else {
        users[data.to].emit('received_message', data);
      }
    });


  });

};


/*
    


    // print current lat long
    socket.on('latlong', function(data) {
      console.log('current lat, long: ' + data);
    });
*/