// server.js

var app = require('express')()
var server = require('http').createServer(app)
var io = require('socket.io').listen(server);
var routes = require('./routes');
var socketmanager = require('./socketmanager');

// routes setup
routes.setup(app);

// socket manager setup
socketmanager(io);

// start server
server.listen(process.env.PORT || 3000);
console.log('server has started');

